import React from 'react';
import PropTypes from 'prop-types';

function Modal  ({ onClose }) {
    return (
        <div className="modal">
            <div className="modal-content">
                <p>Товар додано до кошика!</p>
                <button onClick={onClose} className='btn-modal'>Закрити</button>
            </div>
        </div>
    );
};

Modal.propTypes = {
    onClose: PropTypes.func.isRequired
};

export default Modal;
