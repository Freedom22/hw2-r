import React from 'react';
import PropTypes from 'prop-types';
import ProductCard from './ProductCard';

function ProductList ({ products, onAddToCart, onToggleFavorite }) {
    return (
        <div className="product-list">
            {products.map(product => (
                <ProductCard
                    key={product.sku}
                    product={product}
                    onAddToCart={onAddToCart}
                    onToggleFavorite={onToggleFavorite}
                />
            ))}
        </div>
    );
};

ProductList.propTypes = {
    products: PropTypes.arrayOf(PropTypes.object).isRequired,
    onAddToCart: PropTypes.func.isRequired,
    onToggleFavorite: PropTypes.func.isRequired
};

export default ProductList;
