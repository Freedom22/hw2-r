import React from 'react';
import PropTypes from 'prop-types';


function Header({ cartCount, favoritesCount }) {
    return (
        <header>
            <div className="cart">
               🛒Кошик ({cartCount})
            </div>
            <h1 className='title'>Zara</h1>
            <div className="favorites">
               ★Обрані ({favoritesCount})
            </div>
        </header>
    );
};

Header.propTypes = {
    cartCount: PropTypes.number.isRequired,
    favoritesCount: PropTypes.number.isRequired
};

export default Header;