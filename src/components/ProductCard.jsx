import React from 'react';
import PropTypes from 'prop-types';



function ProductCard  ({ product, onAddToCart, onToggleFavorite })  {
    const { name, price, imageUrl, sku, color, isFavorite = false } = product;

    return (
        <div className="product-card">
            <img src={imageUrl} alt={name} />
            <div className="product-info">
                <h3>{name}</h3>
                <p>Ціна: {price} $</p>
                <p>Колір: {color}</p>
                <button onClick={() => onAddToCart(sku)} className='btn-card'>Add to cart</button>
                <span
                    className={`favorite-icon ${isFavorite ? 'favorite' : ''}`}
                    onClick={() => onToggleFavorite(sku)}
                >
                    ★
                </span>
            </div>
        </div>
    );
};

ProductCard.propTypes = {
    product: PropTypes.shape({
        name: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
        imageUrl: PropTypes.string.isRequired,
        sku: PropTypes.string.isRequired,
        color: PropTypes.string.isRequired,
        isFavorite: PropTypes.bool
    }).isRequired,
    onAddToCart: PropTypes.func.isRequired,
    onToggleFavorite: PropTypes.func.isRequired
};

export default ProductCard;

