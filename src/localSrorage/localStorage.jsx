export const getCartItems = () => JSON.parse(localStorage.getItem('cartItems')) || [];
export const getFavorites = () => JSON.parse(localStorage.getItem('favorites')) || [];
export const saveCartItems = (items) => localStorage.setItem('cartItems', JSON.stringify(items));
export const saveFavorites = (items) => localStorage.setItem('favorites', JSON.stringify(items));
