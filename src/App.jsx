import React, { useState, useEffect } from 'react';

import HomePage from './pages/HomePage';
import { getCartItems, getFavorites } from './localSrorage/localStorage';
import './App.css';

function App() {
    
    const [cartCount, setCartCount] = useState(getCartItems().length);
    const [favoritesCount, setFavoritesCount] = useState(getFavorites().length);

    useEffect(() => {
      const handleStorageChange = () => {
          setCartCount(getCartItems().length);
          setFavoritesCount(getFavorites().length);
      };

      window.addEventListener('storage', handleStorageChange);

      return () => {
          window.removeEventListener('storage', handleStorageChange);
      };
  }, []);

  const updateCartCount = (count) => {
    setCartCount(count);
  };

  const updateFavoritesCount = (count) => {
     setFavoritesCount(count);
   };

  return (
      <div className="App">
         
          <HomePage updateCartCount={updateCartCount} updateFavoritesCount={updateFavoritesCount} />
      </div>
  );
}

export default App
