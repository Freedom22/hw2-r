import React, { useState, useEffect } from 'react';
import ProductList from '../components/ProductList';
import Modal from '../components/Modal';
import Header from '../components/Header';
import { getCartItems, getFavorites, saveCartItems, saveFavorites } from '../localSrorage/localStorage';
import PropTypes from 'prop-types';


function HomePage() {
    const [products, setProducts] = useState([]);
    const [cartItems, setCartItems] = useState(getCartItems());
    const [favorites, setFavorites] = useState(getFavorites());
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(null);

    useEffect(() => {
        fetch('/products.json')
            .then(response => response.json())
            .then(data =>{ 
                setProducts(data)
                setLoading(false)
            })
            .catch(err => {
                setError(err);
                setLoading(false);
            });
    }, []);

    const handleAddToCart = (sku) => {
        const updatedCart = [...cartItems, sku];
        setCartItems(updatedCart);
        saveCartItems(updatedCart);
        updateCartCount(updatedCart.length);
        setIsModalOpen(true);
    };

    const handleToggleFavorite = (sku) => {
        const updatedFavorites = favorites.includes(sku)
            ? favorites.filter(item => item !== sku)
            : [...favorites, sku];
        setFavorites(updatedFavorites);
        saveFavorites(updatedFavorites);
        updateFavoritesCount(updatedFavorites.length);
    };

    const closeModal = () => {
        setIsModalOpen(false);
    };

    const productsWithFavorites = products.map(product => ({
        ...product,
        isFavorite: favorites.includes(product.sku)
    }));
    const updateCartCount = (count) => {
        // Update cart count in the UI or global state
        console.log(`Cart count updated to: ${count}`);
    };

    const updateFavoritesCount = (count) => {
        // Update favorites count in the UI or global state
        console.log(`Favorites count updated to: ${count}`);
    };

    if (loading) {
        return <div>Loading...</div>;
    }

    if (error) {
        return <div>Error: {error.message}</div>;
    }

    return (
        <div className="home-page">
            <Header cartCount={cartItems.length} favoritesCount={favorites.length} />
            <ProductList
                products={productsWithFavorites}
                onAddToCart={handleAddToCart}
                onToggleFavorite={handleToggleFavorite}
            />
            {isModalOpen && <Modal onClose={closeModal} />}
        </div>
    );
};
HomePage.propTypes = {
    products: PropTypes.arrayOf(PropTypes.shape({
        sku: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
        isFavorite: PropTypes.bool
    })),
    onAddToCart: PropTypes.func,
    onToggleFavorite: PropTypes.func,
    isModalOpen: PropTypes.bool,
    onClose: PropTypes.func
};

export default HomePage;
